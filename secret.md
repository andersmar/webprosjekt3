# APACHE SOLR

# IP
# 35.195.222.139

# USERNAME
# user

# PASSWORD
# 4Q0XKrx2BL8n

* https://google.bitnami.com/vms/bitnami-solr-dm-dfc5
* https://docs.bitnami.com/google/infrastructure/solr/
* http://35.195.222.139/solr/#/
* http://lucene.apache.org/solr/guide/7_1/response-writers.html#response-writers

# To create a core on Google cloud
sudo -u solr bin/solr create -c demo

# Go to solr folder
cd /opt/bitnami/apache-solr

# index file
bin/post -c CORENAME example/films/films.json

# index folder
bin/post -c CORENAME example/films/*


# Delete all
bin/post -c localDocs -d "<delete><query>*:*</query></delete>"

# Delete specific doc
bin/post -c localDocs -d "<delete><id>SP2514N</id></delete>"

* http://lucene.apache.org/solr/guide/7_1/schema-factory-definition-in-solrconfig.htmld
# if you have started Solr with managed schema enabled and you would like to switch to manually editing a schema.xml file, you should take the following steps:
1. Rename the managed-schema file to schema.xml.
2. Modify solrconfig.xml to replace the schemaFactory class.
3. Remove any ManagedIndexSchemaFactory definition if it exists.
4. Add a ClassicIndexSchemaFactory definition as shown above
5. Reload the core(s).