<?php

//Makes connection to database
require_once("connect.php");
//function for santitize user input
require_once("./functions/sanitize_user_input.php");
// starts session
require_once("./session/startsession.php");

//If error during submiting error msg is stored here
$wrongUser;

?>

<!DOCTYPE html>
<html>
  <head>
      <meta name="viewport" id="vp" content="initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width" />
      <meta charset="utf-8" />
      <title> Web3 </title>
    	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> <!--bootstrap css -->
      <link rel="stylesheet" type="text/css" href="./css/login.css"> <!--own css-->
  </head>

<?php
// if not logged in
if(!isset($_SESSION['isloggedin'])){
	// if wrong user name and password is provided
	if(isset($_POST['login']))
		$wrongUser = "Wrong username or password!";
	echo <<<END
	<div class="container">
			<div class="row main">
				<div class="main-login main-center">
					<form class="form-horizontal" id="form1" method="post" action="index.php">

						<div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Username</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-user" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="username" id="name"  placeholder="Enter username"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-lock" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="email"  placeholder="Enter password"/>
								</div>
							</div>
						</div>

						<div class="form-group ">
							<button type="submit" form="form1" name="login" value="Log in" class="btn btn-primary btn-lg btn-block login-button">Log in</button>
						</div>

					</form>
				</div>
			</div>
		</div>
	</body>
</html>
END;

if(isset($wrongUser)) {
	echo "<p class='error_msg'>".$wrongUser. "</p>";
	}
}
// if already logged in
else{
	echo <<<END
	<div class="container">
			<div class="row main">
				<div class="main-login main-center">
				<h4>You are logged in!</h4>
				<br>
				<h5>Click <a href='mainpage.php'>here</a> to continue!</h5>
				Do you want to log out?
				<form method="post" id="form2" action="index.php">
					<div class="form-group ">
						<button type="submit" form="form2" name="logout" value="Log out" class="btn btn-primary btn-lg btn-block login-button">Log out</button>
					</div>
				</form>
				</div>
			</div>
		</div>
	</body>
</html>
END;

}

?>
