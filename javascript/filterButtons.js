
// Removes form
$( "#yearButton" ).click(function() {
  $( ".yearForm" ).toggle( "slow", function() {
  });
});

// Changes glyph icons on click
$('#yearButton').on('click', function() {
    $("#yearButton").toggleClass('glyphicon-arrow-up glyphicon-arrow-down');
});

// Adjusts div heigth to missing form on click
$('#yearButton').on('click', function() {
    $(".yearFilterDiv ").toggleClass('yearDivHeight');
});



// Removes keywords
$( "#keywordButton" ).click(function() {
  $( ".keywords" ).toggle( "slow", function() {
  });
});

// Changes glyph icons on click
$('#keywordButton').on('click', function() {
    $("#keywordButton").toggleClass('glyphicon-arrow-up glyphicon-arrow-down');
});

// Adjusts div heigth to missing keywords on click
$('#keywordButton').on('click', function() {
    $(".keywordsFilterDiv ").toggleClass(' keywordFilterHeight1 keywordFilterHeight');
});
