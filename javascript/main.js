// Authenticate
var username = "user",
    password = "4Q0XKrx2BL8n";
// get facets
$(document).ready(function(facets){
    $.ajax({
        url: 'http://35.195.184.3/solr/wp3v2/select?facet.field=keywords&facet=on&fl=keywords&q=*:*',
        type: 'GET',
            dataType: 'jsonp',
            jsonp: 'json.wrf',

        success: function(facets){
            var myStringArray = facets.facet_counts.facet_fields.keywords;
            var arrayLength = myStringArray.length;
            var x = "";
            for (var i = 0; i < arrayLength; i += 2) {
            x += "<a href='' class='keywords'>" + myStringArray[i] + " (" + myStringArray[i+1] + ")</a> ";
            }
            document.getElementById("facets").innerHTML = x;
    }
    })
})

// making query to Solr server
$(function() {

  searchField = $('#search-field');

  // Callback when ajax request is made
  result = function(obj) { /*alert(obj)*/
      console.log(obj);
      var myJSON = JSON.stringify(obj);
      var output = document.getElementById("result");
      console.log(obj.response.docs.length);

      //Stores numbers of results
      var numberOfPdfs = obj.response.docs.length
      //Prints numbers of results to html
      document.getElementById("showingCount").innerHTML ="Displaying 1 to " + numberOfPdfs;
      document.getElementById("showTotalCount").innerHTML ="Found " + numberOfPdfs+ " results";


      // output.innerHTML = myJSON;
      // output.innerHTML = obj.id; */
      // console.log(obj.response.docs[0]); // get array
      // console.log(obj.response.docs[1].title); // Get id from array
      var i, x = "";
      for (i in obj.response.docs) {
          x +=

            "  <div class=' row fileBox filtersDiv' id='result'>"
            + "<div class='col-md-5 col-xs-5'>"
            + "<a class='fileLink' href='"+obj.response.docs[i].id.replace("/opt/bitnami/apache-solr/example/exampledocs/pdf/", "pdf/")+"'>"
            + obj.response.docs[i].id.replace("/opt/bitnami/apache-solr/example/exampledocs/pdf/", "").replace(".pdf", "").replace(/-/g, " ")
            + "</a>"
            +"</div>"
            //  + obj.response.docs[i].author
            + "<div class='col-md-1 col-xs-1 fileType'>"
            + "<p class='fileInfo'>"
            + obj.response.docs[i].content_type[0].replace("application/", "")
            + "</p>"
            + "</div>"
            + "<div class='col-md-3 col-xs-3'>"
            + " <p class='fileInfo'>"
            + obj.response.docs[i].attr_created
            + "</p>"
            + "</div>"
            + "<div class='col-md-3 col-xs-3'>"
            + "<p class='fileInfo'>"
            + obj.response.docs[i].keywords.replace("'","")
            + "</p>"
            //+"<span class='glyphicon glyphicon-plus-sign pull-right' aria-hidden='true' onclick=''></span>"
            +"</div>"

            + "</div>"
      }
      document.getElementById("result").innerHTML = x;
  }


  return $('#search-btn').on('click', function() {
    var search,
        modifier;
    search = searchField.val();
    modifier = "";
    return $.ajax({
      url: 'http://35.195.184.3/solr/wp3v2/select?' + modifier + 'q=' + search,
      success: function(data) {
        return result(data);
      },
      dataType: 'jsonp',
      jsonp: 'json.wrf'
    });
  });

});
// display result (JSON) in document

// https://wiki.apache.org/solr/SolJSON#Using_Solr.27s_JSON_output_for_AJAX
// http://lucene.apache.org/solr/guide/7_1/response-writers.html#json-response-writer
//
