<?php
// connection info
require_once("dbLogIn.php");

//connect to the local host database as root user
try{
  $db = new PDO("mysql:host=$db_host;dbname=$db_database;charset=utf8",
                  $db_user, $db_pass);

} catch(PDOException  $e) {
  die ("Error: ".$e->getMessage());
}

//create new database "news" if it dose not exists
$query = 'CREATE DATABASE IF NOT EXISTS andma_wp3';
if ($db->exec($query)===false)
  die('Query failed:' . $db->errorInfo()[2]);

// select the database
if ($db->exec("USE andma_wp3")===false)
  die('Can not select db:' . $db->errorInfo()[2]);

// creates the table users if it dose not exist
$query = 'CREATE TABLE IF NOT EXISTS users (
  	username VARCHAR(100) PRIMARY KEY NOT NULL,
  	password VARCHAR(255) NOT NULL)';
if ($db->exec($query)===false)
  die('Query failed:' . $db->errorInfo()[2]);

// Create admin user
$setup = $db->prepare('INSERT INTO users (username, password) VALUES (?, ?)');

$setup->execute([
  'admin',
  password_hash('admin123', PASSWORD_DEFAULT)
]);

//send query to server
if ($db->exec($sql_insert)===false)
  die('Query failed:' . $db->errorInfo()[2]);

  ?>
