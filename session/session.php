<?php
session_start();
// check if user logged in and make sure his session is not compromised
if (!isset($_SESSION['isloggedin']) OR $_SESSION['ip'] != $_SERVER['REMOTE_ADDR'] OR $_SESSION['ua'] != $_SERVER['HTTP_USER_AGENT'])
    //sendt to login.php if not logged in
    header('Location: index.php');

// log out
if (isset($_POST['logout'])) {
    // Unset all of the session variables.
    $_SESSION = array();
    // delete the session cookie
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
    }
    // Finally, destroy the session.
    session_destroy();
    // direct user to login
    header('Location: index.php');
}
?>
