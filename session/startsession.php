
<?php

session_start();

if (!isset($_SESSION['isloggedin']) && isset($_POST['login'])) {
	$username = get_post('username', $db);
	$password = strip_tags($_POST['password']);

	$query = "SELECT username,password FROM users WHERE username=?";
	$result = $db->prepare ($query);

	$result->execute(array($username));

	if ($result -> rowCount() != 0) {
		$row = $result -> fetch(PDO::FETCH_ASSOC);
		if (password_verify($password, $row['password'])) {

			// regenerate the session id
			session_regenerate_id();
			// set session parameters
			$_SESSION['username'] = $username;
			$_SESSION['isloggedin'] = TRUE;
			$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
			$_SESSION['ua'] = $_SERVER['HTTP_USER_AGENT'];
		}
	}
}

// log out
if (isset($_POST['logout'])) {
	// Unset all of the session variables.
	$_SESSION = array();
	// delete the session cookie
	if (ini_get("session.use_cookies")) {
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
	}
	// Finally, destroy the session.
	session_destroy();
	// direct user to login
	header('Location: index.php');
}

?>
