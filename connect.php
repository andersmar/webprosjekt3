<?php

//log in info
require_once("dbLogIn.php");

//connect to the local host database as root user
try{
  $db = new PDO("mysql:host=$db_host;dbname=$db_database;charset=utf8",
                  $db_user, $db_pass);
}
catch(PDOException  $e) {
  die ("Error: ".$e->getMessage());
}

// select the database
if ($db->exec("USE andma_wp3")===false)
  die('Can not select db:' . $db->errorInfo()[2]);


?>
