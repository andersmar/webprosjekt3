
<?php 
// sanitize users input
function get_post($var, $conn){
	$var = stripslashes($_POST[$var]);
	$var = htmlentities($var);
	$var = strip_tags($var);
	
	return $var;
}

?>