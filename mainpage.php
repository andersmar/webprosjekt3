<?php

//Makes connection to database
require_once("connect.php");
//function for santitize user input
require_once("./functions/sanitize_user_input.php");
//starts session
require_once("./session/session.php");


// if user is admin send to admin page
if ($_SESSION['username'] != 'admin') {
    header('Location: index.php');
}

?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta name="viewport" id="vp" content="initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width" />
        <meta charset="utf-8" />
        <title> Web3 </title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!--bootstrap css -->
        <link rel="stylesheet" type="text/css" href="./css/styling.css">
        <!--own css-->
    </head>

    <body>

        <!--container -->
        <div class="container-fluid">
            <div class="row contentRow">

                <!--Search and filter coitnainer -->
                <div class="col-md-3 searchFilterCointainer">

                <div id="logo">Petroleumstilsynet</div>
                    <!--Search -->
                    <input type="text" class="searchElements" id="search-field" name="query" value="*" placeholder="Search for titles, year, keywords, etc..">
                    <input id="search-btn" class="btn searchElements" type="submit" value="Search" />
                    <input id="resetButton" class="btn searchElements" type="submit" value="Reset" />
                    <!--Current filters -->
                    <div class="currentFiltersDiv filterDivs">
                        <h4 class="whiteText">Current filters</h4>
                        <!-- Doesn't do anything
            <span class="glyphicon glyphicon-remove whiteText" aria-hidden="true" onclick=""></span>
            <p class="filterParagraph whiteText">Remove all</p> <br>
            <span class="glyphicon glyphicon-remove whiteText" aria-hidden="true" onclick=""></span>
            <p class="filterParagraph whiteText">Keyword</p> <br>
            <span class="glyphicon glyphicon-remove whiteText" aria-hidden="true" onclick=""></span>
            <p class="filterParagraph whiteText">2016</p> <br>
          -->
                    </div>
                    <!--Year filter -->
                    <div class="yearFilterDiv filterDivs yearDivHeight">
                        <h4 class="whiteText">Year</h4>
                        <span id="yearButton" class="yearGlyph glyphicon glyphicon-arrow-down pull-right whiteText" aria-hidden="true" onclick=""></span>
                        <form class="yearForm">
                            <input type="checkbox" name="year" value="2017">
                            <p class="whiteText filterParagraph">2017</p>
                            <p class="filterParagraph whiteText pull-right pNumbers">52</p> <br>
                            <input type="checkbox" name="year" value="2016">
                            <p class="whiteText filterParagraph">2016</p>
                            <p class="filterParagraph whiteText pull-right pNumbers">42</p> <br>
                            <input type="checkbox" name="year" value="2015">
                            <p class="whiteText filterParagraph">2015</p>
                            <p class="filterParagraph whiteText pull-right pNumbers">16</p> <br>
                            <input type="checkbox" name="year" value="2014">
                            <p class="whiteText filterParagraph">2014</p>
                            <p class="filterParagraph whiteText pull-right pNumbers">22</p> <br>
                            <input type="checkbox" name="year" value="2013">
                            <p class="whiteText filterParagraph">2013</p>
                            <p class="filterParagraph whiteText pull-right pNumbers">5</p><br>
                            <input type="checkbox" name="year" value="2012">
                            <p class="whiteText filterParagraph">2012</p>
                            <p class="filterParagraph whiteText pull-right pNumbers">1</p><br>
                        </form>
                    </div>
                    <!--Keywords filter -->
                    <div class="keywordsFilterDiv filterDivs keywordFilterHeight">
                        <h4 class="whiteText">keywords</h4>
                        <span id="keywordButton" class="glyphicon glyphicon-arrow-down pull-right whiteText" aria-hidden="true" onclick=""></span>
                        <p class="keywords whiteText filterParagraph" id="facets"></p>
                    </div>
                </div>

                <!--Result view container -->
                <div class="col-md-9 maincontent">
                    <div class="viewHeadings">
                        <h5 class="col-md-5 col-xs-5">File title</h5>
                        <h5 class="col-md-1 col-xs-1">Type</h5>
                        <h5 class="col-md-3 col-xs-3">Date created</h5>
                        <h5 class="col-md-3 col-xs-3">keywords</h5>
                    </div>
                    <hr>

                    <!--Results printet from solr search here -->
                    <div id="result">
                    </div>
                </div>

                <!--Footer display count -->
                <div class="col-md-9">
                    <div class="displayCount">
                        <p id="showingCount">Displaying 0 to 0 </p>
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true" onclick=""></span>
                        <p>1</p>
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true" onclick=""></span>
                        <p id="showTotalCount">Found 0 results</p>
                    </div>
                </div>

                <!--row end -->
            </div>
            <!--container end -->
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <!--bootstrap javascript -->

        <!--Solr script -->
        <script type='text/javascript' src="./javascript/main.js"></script>

        <!-- JQuery animation scripts -->
        <script type='text/javascript' src='./javascript/filterButtons.js'></script>

    </body>

    </html>